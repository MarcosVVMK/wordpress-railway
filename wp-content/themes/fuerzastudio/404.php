<?php
/**
 * Layout: 404
 *
 * This is the template that is used for displaying 404 errors.
 *
 * @package Fuerza
 */

?>
<p>
	<?php
	printf(
		esc_html__( 'Please check the URL for proper spelling and capitalization. If you\'re having trouble locating a destination, try visiting the %1$shome page%2$s.', 'my_app' ),
		'<a href="' . esc_url( home_url( '/' ) ) . '">',
		'</a>'
	);
	?>
</p>
