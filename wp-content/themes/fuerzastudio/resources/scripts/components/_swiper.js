/**
 * External dependencies.
 */

import $ from 'jquery';

import Swiper from 'swiper';
import { Navigation, Pagination } from 'swiper/modules';

import 'swiper/css';
import 'swiper/css/bundle';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

/**
 * A slider component.
 *
 * @class
 *
 * Relevat links:
 * @link https://arnost.medium.com/creating-custom-slide-transitions-in-swiper-js-also-with-gsap-ac71f9badf53
 */

export default class Slider {
  constructor() {
    this.selector = '.c-slider';

    this.defaultArgs = {
      /**
       * Swiper Modules.
       */
      modules: [Navigation, Pagination],

      slidesPerView: 1,
      slidesPerGroupSkip: 0,
      slidesPerGroup: 1,

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
        formatFractionCurrent(number) {
          return `0${number}`.slice(-2);
        },
        formatFractionTotal(number) {
          return `0${number}`.slice(-2);
        },
        renderFraction(currentClass, totalClass) {
          return (
            `<span class="${currentClass}"></span>`
            + ' | '
            + `<span class="${totalClass}"></span>`
          );
        },
      },
    };
  }

  /**
   * Constructor.
   */
  bootstrap() {
    this.init();
  }

  /**
   * Init.
   *
   * Select DOM Elements, do some logic, etc.
   */
  init() {
    this.$els = $(this.selector);

    this.createSliders();
  }

  /**
   * Create sliders.
   */
  createSliders() {
    const swiperInstance = new Swiper(this.selector, {
      ...this.defaultArgs,
    });

    return swiperInstance;
  }
}
