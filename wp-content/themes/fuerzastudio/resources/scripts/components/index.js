import Example from './_example';
import Embla from './_embla';
import Slider from './_swiper';

export default [new Example(), new Embla(), new Slider()];
