/**
 *
 * Example component.
 *
 */
export default class Example {
  constructor() {
    this.selector = '.c-example';
  }

  bootstrap() {
    this.init();
  }

  init() {
    return this.selector;
  }
}
