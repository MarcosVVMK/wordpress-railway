import EmblaCarousel from 'embla-carousel';

/**
 *
 * Example component.
 *
 */
export default class Embla {
  constructor() {
    this.selector = '.c-embla';

    this.selectors = {
      viewport: '.c-embla__viewport',
      buttonNext: '.c-embla__button--next',
      buttonPrev: '.c-embla__button--prev',
      dots: '.c-embla__dots',
    };

    this.nodes = {
      emblaElement: document.querySelector(this.selector),
      viewportElement: document.querySelector(this.selectors.viewport),
      buttonNextElement: document.querySelector(this.selectors.buttonNext),
      buttonPrevElement: document.querySelector(this.selectors.buttonPrev),
      dotsElement: document.querySelector(this.selectors.dots),
    };

    this.options = {
      loop: false,
    };
  }

  bootstrap() {
    this.initializeEmbla();
  }

  initializeEmbla() {
    const {
      viewportElement,
      buttonPrevElement,
      buttonNextElement,
      dotsElement,
    } = this.nodes;

    const emblaApi = EmblaCarousel(viewportElement, this.options);

    this.addTogglePrevNextBtnsActive(
      emblaApi,
      buttonPrevElement,
      buttonNextElement
    );

    this.addPrevNextBtnsClickHandlers(
      emblaApi,
      buttonPrevElement,
      buttonNextElement
    );

    this.addDotBtnsAndClickHandlers(emblaApi, dotsElement);

    if (emblaApi) {
      emblaApi
        .on('destroy', this.removePrevNextBtnsClickHandlers)
        .on('destroy', this.removeDotBtnsAndClickHandlers)
        .on('resize', () => emblaApi.reInit());
    }
  }

  addTogglePrevNextBtnsActive(emblaApi, prevBtn, nextBtn) {
    const togglePrevNextBtnsState = () => {
      if (emblaApi.canScrollPrev()) prevBtn.removeAttribute('disabled');
      else prevBtn.setAttribute('disabled', 'disabled');

      if (emblaApi.canScrollNext()) nextBtn.removeAttribute('disabled');
      else nextBtn.setAttribute('disabled', 'disabled');
    };

    emblaApi
      .on('select', togglePrevNextBtnsState)
      .on('init', togglePrevNextBtnsState)
      .on('reInit', togglePrevNextBtnsState);

    return () => {
      prevBtn.removeAttribute('disabled');
      nextBtn.removeAttribute('disabled');
    };
  }

  addPrevNextBtnsClickHandlers(emblaApi, prevBtn, nextBtn) {
    const scrollPrev = () => emblaApi.scrollPrev();
    const scrollNext = () => emblaApi.scrollNext();
    prevBtn.addEventListener('click', scrollPrev, false);
    nextBtn.addEventListener('click', scrollNext, false);

    const removeTogglePrevNextBtnsActive = this.addTogglePrevNextBtnsActive(
      emblaApi,
      prevBtn,
      nextBtn
    );

    return () => {
      removeTogglePrevNextBtnsActive();
      prevBtn.removeEventListener('click', scrollPrev, false);
      nextBtn.removeEventListener('click', scrollNext, false);
    };
  }

  addDotBtnsAndClickHandlers(emblaApi, dotsNode) {
    let dotNodes = [];

    const addDotBtnsWithClickHandlers = () => {
      dotsNode.innerHTML = emblaApi
        .scrollSnapList()
        .map(() => '<button class="c-embla__dot" type="button"></button>')
        .join('');

      dotNodes = Array.from(dotsNode.querySelectorAll('.c-embla__dot'));
      dotNodes.forEach((dotNode, index) => {
        dotNode.addEventListener(
          'click',
          () => emblaApi.scrollTo(index),
          false
        );
      });
    };

    const toggleDotBtnsActive = () => {
      const previous = emblaApi.previousScrollSnap();
      const selected = emblaApi.selectedScrollSnap();
      dotNodes[previous].classList.remove('c-embla__dot--selected');
      dotNodes[selected].classList.add('c-embla__dot--selected');
    };

    emblaApi
      .on('init', addDotBtnsWithClickHandlers)
      .on('reInit', addDotBtnsWithClickHandlers)
      .on('init', toggleDotBtnsActive)
      .on('reInit', toggleDotBtnsActive)
      .on('select', toggleDotBtnsActive);

    return () => {
      dotsNode.innerHTML = '';
    };
  }
}
