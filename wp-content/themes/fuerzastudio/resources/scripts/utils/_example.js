/**
 *
 * Example Utils.
 *
 */
export default class Example {
  constructor() {
    this.selector = '.u-example';
  }

  bootstrap() {
    this.init();
  }

  init() {
    return this.selector;
  }
}
