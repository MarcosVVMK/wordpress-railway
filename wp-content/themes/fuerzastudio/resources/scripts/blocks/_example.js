/**
 *
 * Example block.
 *
 */
export default class Example {
  constructor() {
    this.selector = '.b-example';
  }

  bootstrap() {
    this.init();
  }

  init() {
    return this.selector;
  }
}
