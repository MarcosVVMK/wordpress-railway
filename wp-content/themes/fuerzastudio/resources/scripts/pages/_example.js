/**
 *
 * Example page.
 *
 */
export default class Example {
  constructor() {
    this.selector = '.p-example';
  }

  bootstrap() {
    this.init();
  }

  init() {
    return this.selector;
  }
}
