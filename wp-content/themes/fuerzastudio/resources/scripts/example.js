const hello = (name) => `Hello, ${name}! Welcome to our website. Let's code!`;

export default hello;
