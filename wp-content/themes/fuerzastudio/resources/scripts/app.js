import $ from 'jquery';

export default class App {
  constructor({
    components, pages, blocks, utils,
  }) {
    this.initialized = false;
    this.pages = pages;
    this.blocks = blocks;
    this.components = components;
    this.utils = utils;
  }

  bootstrap() {
    do {
      this.init();
    } while (!this.initialized);
  }

  init() {
    $('body').addClass('page-loaded');

    this.loadModules(this.components);
    this.loadModules(this.blocks);
    this.loadModules(this.pages);
    this.loadModules(this.utils);

    this.initialized = true;
  }

  // eslint-disable-next-line class-methods-use-this
  loadModules(modules) {
    if (!modules || modules.length <= 0) {
      return;
    }

    // eslint-disable-next-line no-restricted-syntax
    for (const module of modules) {
      const moduleExists = module.selector && $(module.selector).length > 0;

      if (moduleExists) {
        module.bootstrap();
      }
    }
  }
}
