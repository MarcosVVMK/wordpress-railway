import '../styles/index.scss';

import $ from 'jquery';

import App from './app';
import components from './components';
import blocks from './blocks';
import utils from './utils';
import pages from './pages';

// @ts-ignore
const app = new App({
  components,
  pages,
  blocks,
  utils,
});

$(document).on('ready', () => {
  app.bootstrap();
});
