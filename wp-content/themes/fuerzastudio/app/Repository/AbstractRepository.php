<?php

namespace Fuerza\Repository;

use Fuerza\Interfaces\RepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AbstractRepository responsible for implementing the RepositoryInterface
 */
abstract class AbstractRepository implements RepositoryInterface
{
    /**
     * Model class variable
     *
     * @var
     */
    protected static $model;

    /**
     * Method responsible for loading the model
     *
     * @return Model
     */
    public static function loadModel(): Model
    {
        return new (static::$model);
    }

    /**
     *  Method responsible for returning all records
     *
     * @return Collection
     */
    public static function all(): Collection
    {
        return self::loadModel()::all();
    }

    /**
     *  Method responsible for returning a record by id
     *
     * @param array $attributes Array with the attributes to be searched
     * @return Model|null
     */
    public static function find(array $attributes = []): Model|null
    {
        return self::loadModel()::query()->find($attributes);
    }

    /**
     * Method responsible for creating a record
     *
     * @param array $attributes Array with the attributes to be created
     * @return Model|null
     */
    public static function create(array $attributes = []):Model|null{
        return self::loadModel()::query()->create($attributes);
    }

    /**
     * Method responsible for deleting a record
     *
     * @param int $id Id of the record to be deleted
     * @return bool
     */
    public static function delete(int $id):bool{
        return self::loadModel()::query()->where(['id' => $id])->delete();
    }

    /**
     * Method responsible for updating a record
     *
     * @param int $id Id of the record to be updated
     * @param array $attributes Array with the attributes to be updated
     * @return bool
     */
    public static function update(int $id, array $attributes = []):bool{
        return self::loadModel()::query()->where(['id' => $id])->update($attributes);
    }

}
