<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Usermeta;


/**
 * Class UsermetaRepository responsible for implementing the UsermetaRepositoryInterface
 */
class UsermetaRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Usermeta::class;

    /**
     * Get usermeta by user id
     *
     * @param int $user_id user id
     * @return array
     */
    public static function getUsermetaByUserID(int $user_id ): array
    {
        return self::loadModel()::query()->where(
            [
                'user_id' => $user_id,
            ]
        )->get()->toArray();
    }

    /**
     * Get usermeta by meta key
     *
     * @param string $meta_key meta key
     * @return array
     */
    public static function getUsermetaByKey(string $meta_key ): array
    {
        return self::loadModel()::query()->where(
            [
                'meta_key' => $meta_key,
            ]
        )->get()->toArray();
    }
}
