<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\TermTaxonomy;


/**
 * Class TermTaxonomy Repository responsible for implementing the AbstractRepositoryInterface
 */
class TermTaxonomyRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = TermTaxonomy::class;

    /**
     * Get all TermTaxonomy
     *
     * @return array
     */
    public static function getAllTermTaxonomy(): array
    {
        return self::loadModel()::all()->toArray();
    }
}
