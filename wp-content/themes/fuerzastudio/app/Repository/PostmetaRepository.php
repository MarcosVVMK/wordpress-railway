<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Postmeta;


/**
 * Class PostmetaRepository responsible for implementing the PostmetaRepositoryInterface
 */
class PostmetaRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Postmeta::class;

    /**
     * Get postmeta by post id
     *
     * @param int $post_id post id
     * @return array
     */
    public static function getPostmetaByPostID(int $post_id ): array
    {
        return self::loadModel()::query()->where(
            [
                'post_id' => $post_id,
            ]
        )->get()->toArray();
    }

    /**
     * Get postmeta by meta key
     *
     * @param string $meta_key meta key
     * @return array
     */
    public static function getPostmetaByKey(string $meta_key ): array
    {
        return self::loadModel()::query()->where(
            [
                'meta_key' => $meta_key,
            ]
        )->get()->toArray();
    }
}
