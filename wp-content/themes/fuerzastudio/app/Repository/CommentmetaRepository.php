<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Commentmeta;


/**
 * Class CommentsmetaRepository responsible for implementing the CommentsmetaRepositoryInterface
 */
class CommentmetaRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Commentmeta::class;

    /**
     * Get commentmeta by comments id
     *
     * @param int $comment_id Comment id
     * @return array
     */
    public static function getCommentmetaByCommentID(int $comment_id ): array
    {
        return self::loadModel()::query()->where(
            [
                'comment_id' => $comment_id,
            ]
        )->get()->toArray();
    }

    /**
     * Get commentmeta by meta key function
     *
     * @param string $meta_key Meta key
     * @return array
     */
    public static function getCommentmetaByKey(string $meta_key ): array
    {
        return self::loadModel()::query()->where(
            [
                'meta_key' => $meta_key,
            ]
        )->get()->toArray();
    }
}
