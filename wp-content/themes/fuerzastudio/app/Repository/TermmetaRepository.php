<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Termmeta;


/**
 * Class CommentsmetaRepository responsible for implementing the CommentsmetaRepositoryInterface
 */
class TermmetaRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Termmeta::class;

    /**
     * Get term meta by comments id
     *
     * @param int $meta_id Comment id
     * @return array
     */
    public static function getTermmetaByMetaID(int $meta_id ): array
    {
        return self::loadModel()::query()->where(
            [
                'meta_id' => $meta_id,
            ]
        )->get()->toArray();
    }

    /**
     * Get term meta by meta key function
     *
     * @param string $meta_key Meta key
     * @return array
     */
    public static function getTermmetaByKey(string $meta_key ): array
    {
        return self::loadModel()::query()->where(
            [
                'meta_key' => $meta_key,
            ]
        )->get()->toArray();
    }
}
