<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\TermRelationships;


/**
 * Class TermRelationships Repository responsible for implementing the AbstractRepositoryInterface
 */
class TermRelationshipsRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = TermRelationships::class;

    /**
     * Get all Term Relationships
     *
     * @return array
     */
    public static function getAllTermRelationships(): array
    {
        return self::loadModel()::all()->toArray();
    }
}
