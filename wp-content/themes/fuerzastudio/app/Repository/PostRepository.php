<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Post;

/**
 * Class PostRepository responsible for implementing the PostRepositoryInterface
 */
class PostRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Post::class;

    /**
     * Slug of the post type
     */
    public CONST SLUG = 'post';

    /**
     * Get all posts
     *
     * @return array
     */
    public static function getAllPosts(): array
    {
        return self::loadModel()::query()->where(
            [
                'post_type' => self::SLUG,
                'post_status' => 'publish'
            ]
        )->get()->toArray();
    }
}
