<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Comments;


/**
 * Class CommentsRepository responsible for implementing the CommentsRepositoryInterface
 */
class CommentsRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Comments::class;

    /**
     * Get all comments function
     *
     * @return array
     */
    public static function getAllComments(): array
    {
        return self::loadModel()::all()->toArray();
    }
}
