<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\Terms;


/**
 * Class TermTaxonomy Repository responsible for implementing the AbstractRepositoryInterface
 */
class TermsRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = Terms::class;

    /**
     * Get all terms
     *
     * @return array
     */
    public static function getAllTerms(): array
    {
        return self::loadModel()::all()->toArray();
    }
}
