<?php

namespace Fuerza\Repository;

use Fuerza\Models\Eloquent\User;


/**
 * Class UserRepository responsible for implementing the UserRepositoryInterface
 */
class UserRepository extends AbstractRepository
{

    /**
     * Model class variable
     *
     * @var string
     */
    protected static $model = User::class;

    /**
     * Get all users
     *
     * @return array
     */
    public static function getAllUsers(): array
    {
        return self::loadModel()::all()->toArray();
    }
}
