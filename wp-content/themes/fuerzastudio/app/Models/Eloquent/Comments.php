<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Comments - Eloquent model for the comments table.
 */
class Comments extends Eloquent
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'comments';

    /**
     * Primary key.
     *
     * @var string
     */
    protected string $primary_id = 'comment_ID';

    /**
     * Fillable fields column names.
     *
     * @var string[]
     */
    protected $fillable = [
        'comment_post_ID',
        'comment_author',
        'comment_author_email',
        'comment_contents',
        'comment_date',
    ];

    /**
     *  Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }


}
