<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Post - A model to manage posts table
 */
class Post extends Eloquent
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'posts';

    /**
     * Primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'id';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'post_title',
        'post_type',
        'post_status',
        'post_content',
        'post_author',
        'post_date',
        'post_modified',
    ];

    /**
     * Constructor
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }




}
