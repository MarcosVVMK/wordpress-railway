<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class User - A model to manage users table
 */
class User extends Eloquent
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Required primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'ID';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_login',
        'user_nicename',
        'user_pass',
        'user_email',
        'display_name',
    ];

    /**
     * Constructor
     *
     * @param array $attributes Array of attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }


}
