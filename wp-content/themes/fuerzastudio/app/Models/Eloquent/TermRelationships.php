<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class TermRelationships - A model to manage term_relationships table
 */
class TermRelationships extends Eloquent
{
    /**
     * Database table name for this model
     *
     * @var string
     */
    protected $table = 'term_relationships';

    /**
     * Primary key
     *
     * @var string
     */
    protected string $primary_id = 'object_id';

    /**
     * Fillable fields
     *
     * @var string[]
     */
    protected $fillable = [
        'object_id',
        'term_taxonomy_id',
        'term_order',
    ];

    /**
     * Constructor
     *
     * @param array $attributes Array of attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }

}
