<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Usermeta - A model to manage usermeta table
 */
class Usermeta extends Eloquent
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'usermeta';

    /**
     * Primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'umeta_id';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'umeta_id',
        'user_id',
        'meta_key',
        'meta_value',
    ];

    /**
     * Constructor
     *
     * @param array $attributes Array of attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }


}
