<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Postmeta - A model to manage postmeta table
 */
class Postmeta extends Eloquent
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'postmeta';

    /**
     * Primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'meta_id';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'meta_id',
        'post_id',
        'meta_key',
        'meta_value',
    ];

    /**
     * Constructor
     *
     * @param array $attributes Array of attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }


}
