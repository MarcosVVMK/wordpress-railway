<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Commentmeta - A model to manage commentsmeta table
 */
class Commentmeta extends Eloquent
{
    /**
     *  The table associated with the model.
     *
     * @var string
     */
    protected $table = 'commentmeta';

    /**
     * Primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'meta_id';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'meta_id',
        'comment_id',
        'meta_key',
        'meta_value',
    ];

    /**
     * Constructor
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }


}
