<?php

namespace Fuerza\Models\Eloquent;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class Terms - A model to manage terms table
 */
class Terms extends Eloquent
{
    /**
     * Table name of the model.
     *
     * @var string
     */
    protected $table = 'terms';

    /**
     * Primary key of the table.
     *
     * @var string
     */
    protected string $primary_id = 'term_id';

    /**
     * Fillable fields of the table.
     *
     * @var string[]
     */
    protected $fillable = [
        'term_id',
        'name',
        'slug',
        'term_group',
    ];


    /**
     * Constructor
     *
     * @param array $attributes Array of attributes
     */
    public function __construct(array $attributes = array() ) {

        parent::__construct( $attributes );
    }
}
