<?php

namespace Fuerza\Facades\Api;

use Fuerza\App;

defined( 'ABSPATH' ) || exit;

/**
 * Logging Routes in WordPress REST API.
 */
class RESTAPIRoute {

	/**
	 * Callback for permissions control.
	 *
	 * @var callable
	 */
	private $permission_callback;

	/**
	 * Version for REST API.
	 *
	 * @var string
	 */
	private static string $version = 'v1';

	/**
	 * Group to group routes from the same context.
	 *
	 * @var string
	 */
	private static string $group;

	/**
	 * Save the route name and the route path.
	 *
	 * @var array
	 */
	private static array $routes_in_group;

	/**
	 * Save the full path of the route.
	 *
	 * @var string
	 */
	private string $route;

	/**
	 * Save route endpoint.
	 *
	 * @var string
	 */
	protected string $endpoint;

	/**
	 * Save route method.
	 *
	 * @var string
	 */
	protected string $method;

	/**
	 * Save route args.
	 *
	 * @var array
	 */
	protected array $args;

	/**
	 * Save route callback.
	 *
	 * @var callable
	 */
	protected $callback;

	/**
	 * Registered routes.
	 *
	 * @var array
	 */
	private static array $routes = [];

	/**
	 * Save validate callback.
	 *
	 * @var ?callable
	 */
	private static $validate_callback = null;

	/**
	 * Save middleware callback.
	 *
	 * @var ?callable
	 */
	private $middleware = null;

	/**
	 * Initialize permission.
	 */
	public function __construct() {

		$this->permission( '__return_true' );

	}

	/**
	 * GET route registration.
	 *
	 * @param string   $route    Route path.
	 * @param callable $callback Callback for return.
	 * @param array    $args     Arguments for route registration.
	 *
	 * @return self
	 */
	public static function get( string $route, callable $callback, array $args = [] ): self {

		return ( new static() )->register( \WP_REST_Server::READABLE, $route, $callback, $args );

	}

	/**
	 * POST route registration.
	 *
	 * @param string   $route    Route path.
	 * @param callable $callback Callback for return.
	 * @param array    $args     Arguments for route registration.
	 * 
	 * @return self
	 */
	public static function post( string $route, callable $callback, array $args = [] ): self {

		return ( new static() )->register( \WP_REST_Server::CREATABLE, $route, $callback, $args );

	}

	/**
	 * Register route in WordPress REST API.
	 *
	 * @param string   $method   HTTP method.
	 * @param string   $route    Route path.
	 * @param callable $callback Callback for return.
	 * @param array    $args     Arguments for route registration.
	 * 
	 * @return self
	 */
	private function register( string $method, string $route, callable $callback, array $args = [] ): self {

		if ( ! empty( self::$group ) ) {

			$route = self::$group . $route;
			
		}

		$this->route = '/api/' . self::$version . $route;

		$this->method = $method;

		$this->endpoint = $route;

		$this->callback = $callback;

		$this->middleware = self::$validate_callback;

		$this->args = $args;

		self::$routes[] = $this;

		return $this;

	}

	/**
	 * Register new rest API router.
	 *
	 * @return void
	 */
	public static function registerRoutes(): void {

		foreach ( self::$routes as $route ) {

			register_rest_route(
				'api/' . self::$version,
				$route->endpoint,
				[
					'methods'             => $route->method,
					'callback'            => $route->callback,
					'permission_callback' => $route->permission_callback,
					'args'                => $route->args,
					'validate_callback'   => $route->middleware,
				],
			);

		}

	}

	/**
	 * Set the function that checks the access permission.
	 *
	 * @param callable $permission_callback Callback for permissions control.
	 *
	 * @return self
	 */
	public function permission( callable $permission_callback ): self {

		$this->permission_callback = $permission_callback;

		return $this;

	}

	/**
	 * Set version route group.
	 *
	 * @param string   $version API group version.
	 * @param callable $routes  Callback for routes.
	 *
	 * @return void
	 */
	public static function versionGroup( string $version, callable $routes ): void {

		self::$version = $version;

		$routes();

		self::$version = '';

	}

	/**
	 * Set group for routes from the same context.
	 *
	 * @param string   $group  API group name.
	 * @param callable $routes Callback for routes.
	 *
	 * @return void
	 */
	public static function group( string $group, callable $routes ): void {

		if ( isset( self::$group ) ) {

			$group = self::$group . $group;

		}

		self::$group = $group;

		$routes();

		self::$group = '';

		self::$validate_callback = null;

	}

	/**
	 * Set name for route.
	 *
	 * @param string $route_name Name for route.
	 *
	 * @return self
	 */
	public function name( string $route_name ): self {

		self::$routes_in_group[ $route_name ] = $this->route;

		return $this;

	}

	/**
	 * Get route by name.
	 *
	 * @param string $route_name Name for route.
	 *
	 * @return string
	 */
	public static function getByName( string $route_name ): string {

		if ( ! isset( self::$routes_in_group[ $route_name ] ) ) {

			return '';

		}

		return self::$routes_in_group[ $route_name ];

	}

	/**
	 * Method responsible for adding middleware.
	 *
	 * @param string $middleware Middleware name.
	 * @throws InvalidArgumentException If the middleware does not exist.
	 * 
	 * @return self
	 */
	public static function middleware( string $middleware ): self {

		$middlewares = App::getConfig()['middlewares'] ?? [];

		if ( ! isset( $middlewares[ $middleware ] ) ) {

			throw new \InvalidArgumentException( __( 'Invalid middleware', 'fuerza-studio' ) );

		}

		self::$validate_callback = [ new $middlewares[ $middleware ], 'handle' ];

		return new static();

	}

}
