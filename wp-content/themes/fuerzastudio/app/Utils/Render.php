<?php 

namespace Fuerza\Utils;

/**
 * Class responsible for rededicating views.
 */
class Render {
    
    /**
     * Method responsible for including the view in the template.
     *
     * @param string $view View to be included.
     * @param array  $args Optional arguments.
     * @return void
     */
    public static function view( string $view, array $args = [] ): void {

        if ( empty( $view ) ) {

            return;

        }

        get_template_part( $view, null, array_merge( $args, apply_filters( 'add_view', $view ) ) );

    }

}
