<?php

namespace Fuerza\Utils;

class Acf {
    /**
     * A wrapper around get_field for easier processing of Blade components.
     * Returns empty array if field is not defined.
     *
     * @see https://www.advancedcustomfields.com/resources/get_field/
     *
     * @param string $selector The field name or field key.
     * @param mixed  $post_id The post ID where the value is saved. Defaults to the current post.
     * @param bool   $format_value Whether to apply formatting logic. Defaults to true.
     * @return mixed
     */
    public static function fuerzaField( $selector, $post_id = 0, $format_value = true ) {
        if ( ! function_exists( 'get_field' ) ) {
            return [];
        }

        $field = get_field( $selector, $post_id, $format_value );

        return null !== $field ? $field : [];
    }
}
