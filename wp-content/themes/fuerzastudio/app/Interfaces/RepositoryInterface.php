<?php

namespace Fuerza\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{

    public static function all(): Collection;
    public static function create(array $attributes): Model|null;
    public static function find(array $attributes): Model|null;
    public static function delete(int $id): bool;
    public static function update(int $id, array $attributes): bool;
    public static function loadModel(): Model;
}
