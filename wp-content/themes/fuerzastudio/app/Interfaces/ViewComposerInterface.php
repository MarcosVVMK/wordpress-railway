<?php

namespace Fuerza\Interfaces;

/**
 * Interface responsible for contracting the concrete classes of view composer.
 */
interface ViewComposerInterface {

    /**
     * Method responsible for getting the views.
     *
     * @return array
     */
    public function getViews(): array;

    /**
     * Method responsible for returning variables to the view.
     *
     * @return array
     */
    public function compose(): array;
    
}