<?php

namespace Fuerza\Interfaces;

interface ProviderInterface {
	public function bootstrap(): void;
}
