<?php 

namespace Fuerza\Middleware;

use Fuerza\Middleware\Abstract\AbstractMiddleware;

/**
 * Middleware class for nonce validation.
 */
class Nonce extends AbstractMiddleware {
    
    /**
     * Method that performs nonce validation.
     *
     * @param \WP_REST_Request $request Request Object.
     * @return \WP_Error|boolean
     */
    public function handle( \WP_REST_Request $request ): \WP_Error|bool {

        $nonce = $request->get_param( '_wpnonce' );
        
        if ( empty( $nonce ) ) {

            return $this->getError( 'rest_cookie_invalid_nonce', 'The _wpnonce parameter is required.', 401 );

        }

        return (bool) wp_verify_nonce( $nonce, 'wp_rest' );

    }

}
