<?php 

namespace Fuerza\Middleware;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Fuerza\Middleware\Abstract\AbstractMiddleware;

/**
 * Middleware class for token validation.
 */
class Token extends AbstractMiddleware {
    
    /**
     * Method that performs jwt token validation.
     *
     * @param \WP_REST_Request $request Request Object.
     * @return \WP_Error|boolean
     */
    public function handle( \WP_REST_Request $request ): \WP_Error|bool {

        if ( ! defined( 'JWT_KEY' ) || ! defined( 'JWT_ALG' ) ) {

            return $this->getError( 'rest_invalid_token', 'JWT authentication not configured.', 403 );

		}

        $authorization = $request->get_header( 'authorization' );

        if ( empty( $authorization ) ) {

            return $this->getError( 'rest_invalid_token', 'Authentication token is required.', 401 );

        }

        $jwt = trim( str_replace( 'Bearer', '', $authorization  ) );
        
        try {

           JWT::decode( $jwt, new Key( JWT_KEY, JWT_ALG ) );

           return true;

        } catch ( \Throwable $e ) {

            return $this->getError( 'rest_invalid_token', 'Authentication token is invalid.', 403 );

        }

    }

}
