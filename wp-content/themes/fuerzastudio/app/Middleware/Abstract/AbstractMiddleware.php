<?php 

namespace Fuerza\Middleware\Abstract;

/**
 * Base class for middlewares.
 */
abstract class AbstractMiddleware {

    /**
     * Method to execute the logic of the middleware.
     *
     * @param \WP_REST_Request $request Request Object.
     * @return \WP_Error|boolean
     */
    abstract public function handle( \WP_REST_Request $request ): \WP_Error|bool;
    
    /**
     * Method that returns an error to the middleware.
     *
     * @param string  $error_type  Type of error.
     * @param string  $mensage     Error message.
     * @param integer $status_code Error status code.
     * @return \WP_Error
     */
    protected function getError( string $error_type, string $mensage, int $status_code ): \WP_Error {

        return new \WP_Error(
            $error_type,
            __( $mensage, 'fuerza-studio' ),
            [ 'status' => $status_code ]
        );

    }

}
