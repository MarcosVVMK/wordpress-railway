<?php

namespace Fuerza\ViewComposers\Header;

use Fuerza\Interfaces\ViewComposerInterface;

/**
 * View composer creation example header class.
 */
class CtaExampleHeader implements ViewComposerInterface {

    /**
     * Method responsible for returning views.
     *
     * @return array
     */
    public function getViews(): array {

        return [
            'views/partials/cta-example-header',
        ];
        
    }
    
    /**
     * Method responsible for returning data to the view.
     *
     * @return array
     */
    public function compose(): array {

        return [
            'site_name' => get_bloginfo( 'name' ),
        ];

    }

}