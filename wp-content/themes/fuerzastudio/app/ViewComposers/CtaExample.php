<?php

namespace Fuerza\ViewComposers;

use Fuerza\Interfaces\ViewComposerInterface;

/**
 * View composer creation example class.
 */
class CtaExample implements ViewComposerInterface {

    /**
     * Method responsible for returning views.
     *
     * @return array
     */
    public function getViews(): array {

        return [
            'views/partials/cta-example',
        ];
        
    }
    
    /**
     * Method responsible for returning data to the view.
     *
     * @return array
     */
    public function compose(): array {

        return [
            'site_name' => get_bloginfo( 'name' ),
        ];

    }

}