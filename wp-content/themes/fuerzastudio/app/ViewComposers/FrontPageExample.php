<?php

namespace Fuerza\ViewComposers;

use Fuerza\Interfaces\ViewComposerInterface;

/**
 * Example of creating a view composer for pages.
 */
class FrontPageExample implements ViewComposerInterface {

    /**
     * Method responsible for returning views.
     *
     * @return array
     */
    public function getViews(): array {

        return [
            'front-page',
        ];
        
    }
    
    /**
     * Method responsible for returning data to the view.
     *
     * @return array
     */
    public function compose(): array {

        return [
            'site_name' => get_bloginfo( 'name' ),
        ];

    }

}