<?php

namespace Fuerza\DesignPatterns\Singleton;

/**
 * Class that implements the singleton pattern.
 */
abstract class Singleton {
	
	/**
	 * Unique instances.
	 *
	 * @var array
	 */
	private static array $instances = [];

	/**
	 * Method that blocks object creation.
	 */
    private function __construct() { }

	/**
	 * Method that blocks object cloning.
	 *
	 * @return void
	 */
    private function __clone(): void { }
    
	/**
	 * Method that blocks the unserialize of objects.
	 *
	 * @return void
	 */
    private function __wakeup(): void { }

	/**
	 * Method that creates a single instance of the class.
	 *
	 * @return static
	 */
	final public static function getInstance(): static {

		$className = static::class;

		if ( ! isset( self::$instances[ $className ] ) ) {

			self::$instances[ $className ] = new static();

		}

		return self::$instances[ $className ];

	}

}
