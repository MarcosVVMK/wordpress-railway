<?php

namespace Fuerza\ViewComposersActions;

use Fuerza\DesignPatterns\Singleton\Singleton;

/**
 * Class responsible for adding views composers.
 */
class AddView extends Singleton {

    /**
     * Method responsible for executing the view's compose method.
     *
     * @return void
     */
    public static function addViewData( string $view ): array {
       
        if ( empty( $view ) ) {

            return [];

        }

        $views = Load::getInstance()::getViews();
    
        if ( ! isset( $views[ $view ] ) ) {
    
            return [];
    
        }
    
        return call_user_func( [ $views[ $view ], 'compose' ] );

	}

    /**
     * Method for adding view composer data to page templates.
     *
     * @param string $template Template path.
     * @return string
     */
    public function addTemplateData( string $template ): string {

        $template_data = pathinfo( $template );

        $GLOBALS['args'] = apply_filters( 'add_view', $template_data['filename'] );

        return $template;

    }

}
