<?php

namespace Fuerza\ViewComposersActions;

use Fuerza\Interfaces\ViewComposerInterface;
use Fuerza\DesignPatterns\Singleton\Singleton;

/**
 * Class responsible for loading views composers.
 */
class Load extends Singleton {

    /**
     * Array that contains the views and the object of the class.
     *
     * @var array
     */
    private static array $views = []; 
    
    /**
     * Method responsible for loading and registering the views.
     *
     * @return void
     */
    public static function register(): void {
       
        $directory = self::getDirectory();

		if ( ! is_dir( $directory ) ) {

            return;

        }

        $ri = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $directory ) );

		$namespace = "\\Fuerza\\ViewComposers";

		foreach ( $ri as $path ) {

            if ( ! $path->isFile() ) {
                
                continue;

            }

			$file_name = $path->getBasename( '.php' );

			$class_name = $namespace . str_replace( self::getDirectory(), '', $path->getPath() ) . '\\' . $file_name;

            $class_name = str_replace( '/', '\\', $class_name );
            
            if ( ! class_exists( $class_name ) ) {

				continue;

			}

			try {

				self::setViews( new $class_name() );

			} catch ( \Throwable $e ) {

				continue;

			}

		}

	}

    /**
     * Method responsible for getting the directory.
     *
     * @return string
     */
    private static function getDirectory(): string {

        return get_template_directory() . DIRECTORY_SEPARATOR. 'app' . DIRECTORY_SEPARATOR . 'ViewComposers';

    }

    /**
	 * Method responsible for setting the views.
	 *
	 * @param ViewComposerInterface $class_name Class name.
	 * @return void
	 */
	private static function setViews( ViewComposerInterface $class_name ): void {

        foreach ( $class_name->getViews() as $view ) {

            self::$views[ $view ] = $class_name;

        }

	}

    /**
     * Method responsible for returning the views.
     *
     * @return array
     */
    public static function getViews(): array {

        return self::$views;
        
    }

}