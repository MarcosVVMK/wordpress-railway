<?php 

namespace Fuerza\Traits;

use Firebase\JWT\JWT;
use Firebase\JWT\Key;

/**
 * Trait for JWT token validation.
 */
trait JWTValidate {
    
    /**
     * Method responsible for validating the JWT token.
     *
     * @param \WP_REST_Request $request Request object.
     * @return boolean
     */
    public function validateJWTToken( \WP_REST_Request $request ): bool {

        if ( ! defined( 'JWT_KEY' ) || ! defined( 'JWT_ALG' ) ) {

			return false;
			
		}

        $authorization = $request->get_header( 'authorization' );

        if ( empty( $authorization ) ) {

            return false;

        }

        $jwt = trim( str_replace( 'Bearer', '', $authorization  ) );
        
        try {

           JWT::decode( $jwt, new Key( JWT_KEY, JWT_ALG ) );

           return true;

        } catch ( \Throwable $e ) {
           
            return false;

        }

    } 

}
