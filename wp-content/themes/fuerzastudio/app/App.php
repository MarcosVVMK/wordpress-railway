<?php

namespace Fuerza;

use Fuerza\Interfaces\ProviderInterface;

class App {
	private static array $config = [];

	/**
	 * Boot the app
	 *
	 * @return void
	 */
	public static function boot(): void {
		self::$config = require_once __DIR__ . '/../config/app.php';

		self::registerProviders();
		self::registerContentTypes();
	}

	/**
	 * Register providers
	 *
	 * @throws \Exception
	 * @return void
	 */
	private static function registerProviders(): void {
		foreach ( self::$config['providers'] as $providerClass ) {
			$provider = $providerClass::getInstance();

			if ( ! ( $provider instanceof ProviderInterface ) ) {
				throw new \Exception( 'Invalid provider' );
			}

			$provider->bootstrap();
		}
	}

	/**
	 * Register content types
	 *
	 * @return void
	 */
	private static function registerContentTypes(): void {
		foreach ( ['postTypes', 'taxonomies'] as $configKey ) {
			foreach ( self::$config[$configKey] as $providerClass ) {
				$items = new $providerClass;

				if ( !( $items instanceof ProviderInterface ) ) {
					throw new \Exception( 'Invalid ' . $configKey . ' item' );
				}

				$items->bootstrap();
			}
		}
	}

	/**
	 * Method responsible for obtaining general settings.
	 *
	 * @return array
	 */
	public static function getConfig(): array {

		return self::$config;

	}
	
}
