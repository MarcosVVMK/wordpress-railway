<?php

namespace Fuerza\Providers;

use Fuerza\DesignPatterns\Singleton\Singleton;
use Fuerza\Interfaces\ProviderInterface;
use stdClass;

/**
 * Class responsible for registering and modifying menus
 */
class MenuServiceProvider extends Singleton implements ProviderInterface {
    /**
     * Method responsible for registering nav menu hooks
     *
     * @return void
     */
    public function bootstrap(): void {
        add_action( 'after_setup_theme', [ $this, 'registerMenus' ] );
        add_filter( 'nav_menu_css_class', [ $this, 'changeMenuClasses' ], 10, 3 );
        add_filter( 'nav_menu_submenu_css_class', [ $this, 'changeUnusedSubMenuClasses' ], 10, 2 );
        add_filter( 'nav_menu_link_attributes', [ $this, 'addAdditionalAttributes'], 1, 3);
        add_filter( 'walker_nav_menu_start_el', [ $this, 'addDescriptionToMenu'], 10, 4);
    }

    /**
     *  Method responsible for registering menus in WordPress.
     *
     * @return void
     */
    public function registerMenus(): void {
        register_nav_menus( [
            'header' => __( 'Header', 'fuerza-studio' ),
            'footer' => __( 'Footer', 'fuerza-studio' ),
        ] );
    }

    /**
     *  Method responsible for removing unnecessary classes from the menu and adding additional li classes.
     *
     * @param array $classes Nav menu classes.
     * @param mixed $item Nav menu item.
     * @param stdClass $args Nav menu arguments.
     * @return array
     */
    public function changeMenuClasses( array $classes, mixed $item, stdClass $args ): array {

        if ( isset( $args->theme_location ) && $args->theme_location === 'footer' ) {

            return $classes;

        }

        foreach ($classes as $key => $class) {

            if ( in_array( $class, $this->classesToRemove( $item ),  ) ) {

               unset($classes[$key]);

            }

        }

        if ( isset($args->add_li_class) ) {

            $classes[] = $args->add_li_class;

        }

        return $classes;
    }

    /**
     * Method responsible for changing the class of the submenu.
     *
     * @param array $classes Nav menu css classes.
     * @param stdClass $args Nav menu arguments.
     * @return array
     */
    public function changeUnusedSubMenuClasses( array $classes, stdClass $args ): array {

        if ( isset( $args->theme_location ) && $args->theme_location === 'footer' ) {
            return $classes;
        }

        foreach ($classes as $key => $class) {
           if ( $class === 'sub-menu' ) {
              $classes[$key] = 'dropdown-menu';
           }
        }

        return $classes;
    }

    /**
     * Method responsible for adding additional attributes to the menu.
     *
     * @param array $atts Nav menu attributes.
     * @param mixed $item Nav menu item.
     * @param stdClass $args Nav menu arguments.
     * @return array
     */
    public function addAdditionalAttributes( array $atts, mixed $item, stdClass $args ): array {

        if ( isset( $args->theme_location ) && $args->theme_location === 'footer' ) {
            return $atts;

        }

        if ( isset( $args->add_a_class ) ) {
            $atts['class'] = isset( $atts['class'] ) ? $atts['class'] . ' ' . $args->add_a_class : $args->add_a_class;
        }

        if ( in_array( 'menu-item-has-children', $item->classes )  ){
            $atts['class']        .= ' dropdown-toggle';
            $atts['role']          = 'button';
            $atts['aria-haspopup'] = 'true';
            $atts['aria-expanded'] = 'false';
            $atts['id']            = 'navbarDropdown';
            $atts['aria-label']    = 'Dropdown menu items';

            if ( in_array( 'menu-item-type-wpml_ls_menu_item', $item->classes ) ) {
                $atts['class'] = 'nav-link dropdown-toggle';
            }
       }

        return $atts;
    }

    /**
     * @param string $item_output Nav menu item output.
     * @param mixed $item Nav menu item.
     * @param int $depth Nav menu depth.
     * @param stdClass $args Nav menu arguments.
     * @return string
     */
    public function addDescriptionToMenu( string $item_output, mixed $item, int $depth, stdClass $args ): string {


        if ( isset( $args->add_description ) ) {
            $item_output .= '<span class="menu-item-description">' . $item->description . '</span>';
        }

        return $item_output;
    }

    /**
     * Method responsible for returning classes to remove from the menu.
     *
     * @param mixed $item Nav menu item.
     * @return string[]
     */
    private function classesToRemove( mixed $item ): array {
        if ( isset( $item->theme_location ) && $item->theme_location === 'footer' ) {
            return [];
        }

        return [
            'menu-item',
            'menu-item-type-post_type',
            'menu-item-object-page',
            'menu-item-' . $item->ID,
            'menu-item-type-custom',
            'menu-item-object-custom',
            'menu-item-has-children',
        ];
    }
}

