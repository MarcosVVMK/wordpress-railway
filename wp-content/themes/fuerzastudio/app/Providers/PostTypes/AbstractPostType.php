<?php
namespace Fuerza\Providers\PostTypes;

use Fuerza\DesignPatterns\Singleton\Singleton;
use Fuerza\Interfaces\ProviderInterface;

/**
 * Abstract class to implement default logic in Custom Post Types.
 *
 * Uses Singleton pattern to instantiate the inherited classes.
 */
abstract class AbstractPostType extends Singleton implements ProviderInterface {

	/**
	 * Text domain for translations
	 *
	 * @var string
	 */
	private $textdomain = 'fuerza-studio';

	/**
	 * CPT slug. Used in register_post_type
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * CPT Singular Name. Used in Labels of register_post_type
	 *
	 * @var string
	 */
	protected $singular;

	/**
	 * CPT Plural Name. Used in Labels of register_post_type
	 *
	 * @var string
	 */
	protected $plural;

	public function bootstrap(): void {
		add_action( 'init', [ $this, 'registerPostType' ] );
		add_action( 'init', [ $this, 'addCustomCapabilities' ] );
	}

	/**
	 * Register post type with default options.
	 *
	 * Use the $args to customize the default options in inherited classes
	 *
	 * @param array $args register_post_type arguments.
	 * @return void
	 */
	public function registerPostType( $args = [] ) {
		$defaults = [
			'labels'          => $this->getPostTypeLabels(),
			'public'          => true,
			'show_in_menu'    => true,
			'hierarchical'    => false,
			'supports'        => [ 'title', 'editor', 'excerpt', 'thumbnail' ],
			'capability_type' => [ strtolower( $this->slug ), strtolower( $this->slug . 's' ) ],
			'has_archive'     => true,
			'show_in_rest'    => true,
		];

		$args = wp_parse_args( $args, $defaults );

		register_post_type( $this->slug, $args );
	}

	/**
	 * Add post type custom capabilities to Administrator and Editor WordPress roles
	 *
	 * @return void
	 */
	public function addCustomCapabilities() {
		$this->addCapabilitiesToRole( 'administrator', $this->getAllPostTypeCapabilitiesMapped( strtolower( $this->slug ), strtolower( $this->slug . 's' ) ) );
		$this->addCapabilitiesToRole( 'editor', $this->getAllPostTypeCapabilitiesMapped( strtolower( $this->slug ), strtolower( $this->slug . 's' ) ) );
	}

	/**
	 * Customize the enter title field for a custom post type
	 *
	 * @return string
	 */
	public function getEnterTitleText() {
		/* translators: %s: post type singular name */
		return sprintf( __( 'Add %s title', 'Fuerza' ), $this->singular );
	}

	/**
	 * Returns the post_type slug
	 *
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * Returns the post_type plural name
	 *
	 * @return string
	 */
	public function getPluralName() {
		return $this->plural;
	}

	/**
	 * Get the labels customized for the custom post type
	 *
	 * @return array
	 */
	private function getPostTypeLabels() {
		return [
			'name'                     => $this->plural,
			'singular_name'            => $this->singular,
			'add_new'                  => __( 'Add New', $this->textdomain ),
			/* translators: %s: post type singular name */
			'add_new_item'             => sprintf( __( 'Add new %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'edit_item'                => sprintf( __( 'Edit %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'new_item'                 => sprintf( __( 'New %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'view_item'                => sprintf( __( 'View %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type plural name */
			'view_items'               => sprintf( __( 'View %s', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'search_items'             => sprintf( __( 'Search %s', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'not_found'                => sprintf( __( 'No %s found', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'not_found_in_trash'       => sprintf( __( 'No %s found in Trash', $this->textdomain ), $this->plural ),
			/* translators: %s: post type singular name */
			'parent_item_colon'        => sprintf( __( 'Parent %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type plural name */
			'all_items'                => sprintf( __( 'All %s', $this->textdomain ), $this->plural ),
			/* translators: %s: post type singular name */
			'archives'                 => sprintf( __( '%s Archives', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'attributes'               => sprintf( __( '%s Attributes', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'insert_into_item'         => sprintf( __( 'Insert into %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'uploaded_to_this_item'    => sprintf( __( 'Uploaded to this %s', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'featured_image'           => sprintf( __( '%s featured image', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'set_featured_image'       => sprintf( __( 'Set %s featured image', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'remove_featured_image'    => sprintf( __( 'Remove %s featured image', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'use_featured_image'       => sprintf( __( 'Use as %s featured image', $this->textdomain ), $this->singular ),
			'menu_name'                => $this->plural,
			/* translators: %s: post type plural name */
			'filter_items_list'        => sprintf( __( 'Filter %s list', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'filter_by_date'           => sprintf( __( 'Filter %s by date', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'items_list_navigation'    => sprintf( __( '%s list navigation', $this->textdomain ), $this->plural ),
			/* translators: %s: post type plural name */
			'items_list'               => sprintf( __( '%s list', $this->textdomain ), $this->plural ),
			/* translators: %s: post type singular name */
			'item_published'           => sprintf( __( '%s published', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_published_privately' => sprintf( __( '%s published privately', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_reverted_to_draft'   => sprintf( __( '%s reverted to draft', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_scheduled'           => sprintf( __( '%s scheduled', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_updated'             => sprintf( __( '%s updated', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_link'                => sprintf( __( '%s link', $this->textdomain ), $this->singular ),
			/* translators: %s: post type singular name */
			'item_link_description'    => sprintf( __( 'A link to a %s', $this->textdomain ), $this->singular ),
		];
	}

	/**
	 * Add custom capabilities to a role
	 *
	 * @param string       $role_name The name for the role.
	 * @param string|array $capabilities The capabilities to attach to role.
	 * @return void|bool
	 */
	public function addCapabilitiesToRole( $role_name, $capabilities ) {
		if ( ! is_array( $capabilities ) ) {
			$role = get_role( $role_name );
			return $role->add_cap( $capabilities );
		}

		foreach ( $capabilities as $cap ) {
			$this->addCapabilitiesToRole( $role_name, $cap );
		}
	}

	/**
	 * Retrieves all custom post type capabilites
	 *
	 * @param string $singular Singular text.
	 * @param string $plural Plurar text.
	 * @return array
	 */
	public function getAllPostTypeCapabilitiesMapped( $singular, $plural ) {
		return [
			'edit_post'              => "edit_$singular",
			'read_post'              => "read_$singular",
			'delete_post'            => "delete_$singular",
			'edit_posts'             => "edit_$plural",
			'edit_others_posts'      => "edit_others_$plural",
			'publish_posts'          => "publish_$plural",
			'read_private_posts'     => "read_private_$plural",
			'read'                   => 'read',
			'delete_posts'           => "delete_$plural",
			'delete_private_posts'   => "delete_private_$plural",
			'delete_published_posts' => "delete_published_$plural",
			'delete_others_posts'    => "delete_others_$plural",
			'edit_private_posts'     => "edit_private_$plural",
			'edit_published_posts'   => "edit_published_$plural",
			'create_posts'           => "edit_$plural",
		];
	}
}
