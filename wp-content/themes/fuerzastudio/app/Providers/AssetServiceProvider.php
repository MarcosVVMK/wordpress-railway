<?php

namespace Fuerza\Providers;

use Fuerza\DesignPatterns\Singleton\Singleton;
use Fuerza\Interfaces\ProviderInterface;

class AssetServiceProvider extends Singleton implements ProviderInterface {
	private string $distPath;

	public function __construct() {
		$this->distPath = get_stylesheet_directory_uri() . '/dist/';
	}

	/**
	 * Bootstrap the provider
	 *
	 * @return void
	 */
	public function bootstrap(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'registerScripts' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'registerStyles' ] );
		add_filter( 'body_class', [ $this, 'bodyClass' ] );
	}

	/**
	 * Register scripts
	 *
	 * @return void
	 */
	public function registerScripts(): void {
		$this->enqueueScript(
			'theme-script-bundle',
			$this->getScriptUrl( 'index.js' ),
			[ 'jquery', 'wp-api-fetch', 'wp-url', 'wp-element', 'wp-data' ],
			true
		);
	}

	/**
	 * Register styles
	 *
	 * @return void
	 */
	public function registerStyles(): void {
		$this->enqueueStyle(
			'theme-style-bundle',
			$this->getScriptUrl( 'index.css' )
		);
	}

	/**
	 * Get script url
	 *
	 * @return string
	 */
	public function getScriptUrl( string $scriptName ): string {
		return $this->distPath . $scriptName;
	}

	/**
	 * Enqueue script
	 *
	 * @return void
	 */
	public function enqueueScript( string $handler, string $fileSource, array $dependency = [], bool $inFooter = false ): void {
		wp_enqueue_script(
			$handler,
			$fileSource,
			$dependency,
			$this->generateScriptVersion( $fileSource ),
			$inFooter
		);
	}

	/**
	 * Enqueue style
	 *
	 * @return void
	 */
	public function enqueueStyle( string $handler, string $fileSource, array $dependency = [], string $media = 'all' ): void {
		wp_enqueue_style(
			$handler,
			$fileSource,
			$dependency,
			$this->generateScriptVersion( $fileSource ),
			$media
		);
	}

	/**
	 * Generate script version
	 *
	 * @return string
	 */
	public function generateScriptVersion( string $fileSource ): string {
		$file = str_replace( get_stylesheet_directory_uri(), get_stylesheet_directory(), $fileSource );

		if ( ! file_exists( $file ) ) {
			return '';
		}

		return md5( filemtime( $file ) );
	}

	/**
	 * Body Class Hook
	 *
	 * @param array $class_names Class names.
	 * @return array
	 */
	public function bodyClass( $class_names ) {
		$current_lang = apply_filters( 'wpml_current_language', null );

		if ( $current_lang ) {
			$class_names[] = 'lang-' . $current_lang;
		}

		return $class_names;
	}
}
