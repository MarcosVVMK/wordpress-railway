<?php

namespace Fuerza\Providers;

use Fuerza\ViewComposersActions\Load;
use Fuerza\ViewComposersActions\AddView;
use Fuerza\Interfaces\ProviderInterface;
use Fuerza\DesignPatterns\Singleton\Singleton;

/**
 * Class responsible for registering view composers.
 */
class ViewComposerServiceProvider extends Singleton implements ProviderInterface {
    
    /**
     * {@inheritDoc}
     */
    public function bootstrap(): void {

      Load::getInstance()::register();

      add_filter( 'add_view', [ AddView::getInstance(), 'addViewData' ], 20 );

      add_filter( 'template_include', [ AddView::getInstance(), 'addTemplateData' ], 99 );

    }

}
