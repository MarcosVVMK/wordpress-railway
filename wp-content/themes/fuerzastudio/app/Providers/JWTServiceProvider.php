<?php

namespace Fuerza\Providers;

use Firebase\JWT\JWT;
use Fuerza\Facades\Api\RESTAPIRoute;
use Fuerza\Interfaces\ProviderInterface;
use Fuerza\DesignPatterns\Singleton\Singleton;

/**
 * Register jwt routes.
 */
class JWTServiceProvider extends Singleton implements ProviderInterface {

	/**
	 * Route namespace.
	 */
	const ROUTE_NAMESPACE = '/jwt/authenticate';

	/**
	 * {@inheritDoc}
	 */
	public function bootstrap(): void {

		RESTAPIRoute::post(
			self::ROUTE_NAMESPACE,
			[ $this, 'authenticate' ]
		);

	}

	/**
	 * Method responsible for authentication and token return.
	 *
	 * @return void
	 */
	public function authenticate( \WP_REST_Request $request ): void {

		if ( ! defined( 'JWT_KEY' ) || ! defined( 'JWT_ALG' ) || ! defined( 'JWT_EXP' ) ) {

			wp_send_json( [ 'message' => __( 'JWT configuration failure', 'fuerza-studio' ) ], 401 );
			
		}

		$email = $request->get_param( 'email' );

		$password = $request->get_param( 'password' );

		$authenticate = wp_authenticate( $email, $password );

		if ( $authenticate instanceof \WP_Error ) {

			wp_send_json( [ 'message' => __( 'Failed to authenticate', 'fuerza-studio' ) ], 401 );
		
		}

		$payload = [
			'iat'     => time(),
			'exp'     => time() + JWT_EXP,
			'user_id' => $authenticate->ID,
		];
		
		$jwt = JWT::encode( $payload, JWT_KEY, JWT_ALG );

		wp_send_json( [ 'token' => $jwt ] );

	}

}
