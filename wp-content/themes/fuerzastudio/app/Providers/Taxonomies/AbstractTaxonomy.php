<?php
namespace Fuerza\Providers\Taxonomies;

use Fuerza\DesignPatterns\Singleton\Singleton;
use Fuerza\Interfaces\ProviderInterface;

/**
 * Abstract class to implement default logic in Taxonomies
 *
 * Uses Singleton pattern to instantiate the inherited classes.
 */
abstract class AbstractTaxonomy extends Singleton implements ProviderInterface {

	/**
	 * Text domain for translations
	 *
	 * @var string
	 */
	private $textdomain = 'fuerza-studio';

	/**
	 * Taxonomy slug. Used in register_taxonomy
	 *
	 * @var string
	 */
	protected $slug;

	/**
	 * Custom Post Types to the taxonomy. Used in register_taxonomy
	 *
	 * @var string|array
	 */
	protected $post_types;

	/**
	 * Taxonomy Singular Name. Used in Labels of register_taxonomy
	 *
	 * @var string
	 */
	protected $singular;

	/**
	 * Taxonomy Plural Name. Used in Labels of register_taxonomy
	 *
	 * @var string
	 */
	protected $plural;

	/**
	 * Class construct, define here the protected attributes from AbstractTaxonomyController class
	 */
	public function bootstrap(): void {
		add_action( 'init', [ $this, 'registerTaxonomy' ] );
		add_action( 'init', [ $this, 'addCustomCapabilities' ] );
	}

	/**
	 * Register taxonomy with default options.
	 *
	 * Use the $args to customize the default options in inherited classes
	 *
	 * @param array $args register_taxonomy arguments.
	 * @return void
	 */
	public function registerTaxonomy( $args = [] ) {
		$defaults = [
			'labels'            => $this->getTaxonomyLabels(),
			'public'            => true,
			'show_ui'           => true,
			'hierarchical'      => true,
			'show_admin_column' => true,
			'show_in_rest'      => true,
			'capabilities'      => $this->getAllTaxonomyCapabilitiesMapped( strtolower( $this->slug . 's' ) ),
		];

		$args = wp_parse_args( $args, $defaults );

		register_taxonomy( $this->slug, $this->post_types, $args );
	}

	/**
	 * Add taxonomy capabilities to Administrator and Editor WordPress roles
	 *
	 * @return void
	 */
	public function addCustomCapabilities() {
		$this->addCapabilitiesToRole( 'administrator', $this->getAllTaxonomyCapabilitiesMapped( strtolower( $this->slug . 's' ) ) );
		$this->addCapabilitiesToRole( 'editor', $this->getAllTaxonomyCapabilitiesMapped( strtolower( $this->slug . 's' ) ) );
	}

	/**
	 * Returns the taxonomy slug
	 *
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * Returns the taxonomy plural name
	 *
	 * @return string
	 */
	public function getPluralName() {
		return $this->plural;
	}

	/**
	 * Get the labels customized for the Taxonomy
	 *
	 * @return array
	 */
	private function getTaxonomyLabels() {
		return [
			'name'                       => $this->plural,
			'singular_name'              => $this->singular,
			/* translators: %s: taxonomy plural name */
			'search_items'               => sprintf( __( 'Search %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'popular_items'              => sprintf( __( 'Popular %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy singular name */
			'all_items'                  => sprintf( __( 'All %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy singular name */
			'parent_item'                => sprintf( __( 'Parent %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'parent_item_colon'          => sprintf( __( 'Parent: %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'edit_item'                  => sprintf( __( 'Edit %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'view_item'                  => sprintf( __( 'View %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'update_item'                => sprintf( __( 'Update %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'add_new_item'               => sprintf( __( 'Add New %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'new_item_name'              => sprintf( __( 'New %s Name', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy plural name */
			'separate_items_with_commas' => sprintf( __( 'Separate %s with commas', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'add_or_remove_items'        => sprintf( __( 'Add or remove %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'not_found'                  => sprintf( __( 'No %s found', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'no_terms'                   => sprintf( __( 'No %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy singular name */
			'filter_by_item'             => sprintf( __( 'Filter by %s', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy plural name */
			'items_list_navigation'      => sprintf( __( '%s list navigation', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'items_list'                 => sprintf( __( '%s list', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'most_used'                  => sprintf( __( 'Most Used %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy plural name */
			'back_to_items'              => sprintf( __( 'Back to %s', $this->textdomain ), $this->plural ),
			/* translators: %s: taxonomy singular name */
			'item_link'                  => sprintf( __( '%s Link', $this->textdomain ), $this->singular ),
			/* translators: %s: taxonomy singular name */
			'item_link_description'      => sprintf( __( 'A link to a %s', $this->textdomain ), $this->singular ),
		];
	}

	/**
	 * Get all taxonomy capabilities mapped
	 *
	 * @param string $taxonomy Taxonomy slug.
	 * @return array
	 */
	public function getAllTaxonomyCapabilitiesMapped( $taxonomy ) {
		return [
			'manage_terms' => "manage_$taxonomy",
			'edit_terms'   => "edit_$taxonomy",
			'delete_terms' => "delete_$taxonomy",
			'assign_terms' => "assign_$taxonomy",
		];
	}

	/**
	 * Add custom capabilities to a role
	 *
	 * @param string       $role_name The name for the role.
	 * @param string|array $capabilities The capabilities to attach to role.
	 * @return void|bool
	 */
	public function addCapabilitiesToRole( $role_name, $capabilities ) {
		if ( ! is_array( $capabilities ) ) {
			$role = get_role( $role_name );
			return $role->add_cap( $capabilities );
		}

		foreach ( $capabilities as $cap ) {
			$this->addCapabilitiesToRole( $role_name, $cap );
		}
	}
}
