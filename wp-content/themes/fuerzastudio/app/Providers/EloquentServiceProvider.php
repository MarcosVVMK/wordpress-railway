<?php

namespace Fuerza\Providers;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher as Dispatcher;
use Illuminate\Container\Container as Container;

use Fuerza\DesignPatterns\Singleton\Singleton;
use Fuerza\Interfaces\ProviderInterface;

class EloquentServiceProvider extends Singleton implements ProviderInterface
{
    public function bootstrap(): void
    {
        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => DB_NAME,
            'username'  => DB_USER,
            'password'  => DB_PASSWORD,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => TABLE_PREFIX,
        ]);

        $capsule->setEventDispatcher( new Dispatcher ( new Container ) );
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }


}
