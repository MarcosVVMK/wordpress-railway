<?php

namespace Fuerza\Providers;

use Fuerza\Facades\Api\RESTAPIRoute;
use Fuerza\Interfaces\ProviderInterface;
use Fuerza\DesignPatterns\Singleton\Singleton;

/**
 * Register routes.
 */
class RouteServiceProvider extends Singleton implements ProviderInterface {

	/**
	 * {@inheritDoc}
	 */
	public function bootstrap(): void {

		add_action( 'rest_api_init', [ new RESTAPIRoute, 'registerRoutes' ] );

	}

}
