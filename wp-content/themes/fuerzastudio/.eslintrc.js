/* eslint-disable */
module.exports = {
  'root': true,
  'parserOptions': {
    'ecmaVersion': 11,
  },
  'extends': [
    'airbnb',
    'plugin:import/recommended'
  ],
  'globals': {
    'wp': true,
    'acf': true,
    'fuerzastudio': true,
  },
  'env': {
    'node': true,
    'es6': true,
    'amd': true,
    'browser': true,
    'jquery': true,
  },
  'settings': {
    'import/core-modules': [],
    'import/ignore': [
      'node_modules',
      '\\.(coffee|scss|css|less|hbs|svg|json)$',
    ],
  },
  'plugins': [
    'import'
  ],
  'rules': {
    'quotes': ['error', 'single'],
    'class-methods-use-this': 'off',
    'no-param-reassign': 'off',
    'comma-dangle': [
      'error',
      {
        'arrays': 'always-multiline',
        'objects': 'always-multiline',
        'imports': 'always-multiline',
        'exports': 'always-multiline',
        'functions': 'ignore',
      },
    ],
    'linebreak-style': 0,
    'no-restricted-syntax': 0,
    'id-length': [2, {
      'min': 2,
      'max': Number.infinity,
      'properties': 'always',
      'exceptions': ['_', 'i', 'j', 'x', 'y', 'z', '$']
    }],
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],
    'dot-location': ['error', 'property'],
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    'import/first': 'off',
    'import/prefer-default-export': 'off',
  }
};
