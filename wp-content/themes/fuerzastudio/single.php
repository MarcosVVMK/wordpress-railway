<?php
/**
 * Layout: Single
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fuerza
 */

 get_template_part( 'views/partials/loop-single' );
 get_template_part( 'views/partials/loop' );
