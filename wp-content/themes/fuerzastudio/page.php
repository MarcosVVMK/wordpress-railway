<?php
/**
 * Layout: page
 *
 * This is the template that is used for displaying all pages by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fuerza
 */

?>
<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
	<div <?php post_class(); ?>>
		<h2 class="post-title"><?php the_title(); ?></h2>

		<div class="page__content">
			<?php the_content(); ?>

			<?php edit_post_link( __( 'Edit this entry.', 'fuerza-studio' ), '<p>', '</p>' ); ?>

			<?php get_template_part( 'views/partials/pagination' ); ?>
		</div>
	</div>
<?php endwhile; ?>
