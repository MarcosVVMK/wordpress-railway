<?php
/**
 * Sidebar.
 *
 * @link https://codex.wordpress.org/Customizing_Your_Sidebar
 *
 * @package Fuerza
 */

?>
<div class="sidebar">
	<ul class="widgets">
		<?php get_sidebar(); ?>
	</ul>
</div>
