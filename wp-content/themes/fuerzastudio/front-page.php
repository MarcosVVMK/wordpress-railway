<?php

/**
 * Front Page Template
 *
 * The template for displaying the front page.
 *
 * @package Fuerza
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php
        if (have_posts()):
            while (have_posts()):
                the_post();
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                    <header class="entry-header">
                        <h2 class="entry-title">
                            <div class="c-example" style="margin-top:50px">
                                Olá mundo
                            </div>

                            <section style="margin-top:50px;margin-bottom:50px;">
                                <div class="c-embla">
                                    <div class="c-embla__viewport">
                                        <div class="c-embla__container">
                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>
                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>

                                            <div class="c-embla__slide">
                                                <img src="https://encurtador.com.br/cCDU5" alt="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="c-embla__buttons">
                                        <button class="c-embla__button c-embla__button--prev" type="button" disabled="">
                                            <svg class="c-embla__button__svg" viewBox="0 0 532 532">
                                                <path fill="currentColor"
                                                    d="M355.66 11.354c13.793-13.805 36.208-13.805 50.001 0 13.785 13.804 13.785 36.238 0 50.034L201.22 266l204.442 204.61c13.785 13.805 13.785 36.239 0 50.044-13.793 13.796-36.208 13.796-50.002 0a5994246.277 5994246.277 0 0 0-229.332-229.454 35.065 35.065 0 0 1-10.326-25.126c0-9.2 3.393-18.26 10.326-25.2C172.192 194.973 332.731 34.31 355.66 11.354Z">
                                                </path>
                                            </svg>
                                        </button>

                                        <button class="c-embla__button c-embla__button--next" type="button" disabled="">
                                            <svg class="embla__button__svg" viewBox="0 0 532 532">
                                                <path fill="currentColor"
                                                    d="M176.34 520.646c-13.793 13.805-36.208 13.805-50.001 0-13.785-13.804-13.785-36.238 0-50.034L330.78 266 126.34 61.391c-13.785-13.805-13.785-36.239 0-50.044 13.793-13.796 36.208-13.796 50.002 0 22.928 22.947 206.395 206.507 229.332 229.454a35.065 35.065 0 0 1 10.326 25.126c0 9.2-3.393 18.26-10.326 25.2-45.865 45.901-206.404 206.564-229.332 229.52Z">
                                                </path>
                                            </svg>
                                        </button>

                                    </div>
                                    <div class="c-embla__dots"></div>
                                </div>
                            </section>

                            <div class="c-slider swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">Slide 1</div>
                                    <div class="swiper-slide">Slide 2</div>
                                    <div class="swiper-slide">Slide 3</div>
                                    ...
                                </div>
                                <div class="swiper-pagination"></div>

                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>


                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    </header>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </article>
                <?php
            endwhile;
        else:
            ?>
            <p>
                <?php _e('No posts found.', 'fuerza-studio'); ?>
            </p>
            <?php
        endif;
        ?>

    </main>
</div>

<?php
get_sidebar();
get_footer();