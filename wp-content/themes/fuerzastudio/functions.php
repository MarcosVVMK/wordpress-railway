<?php

use Fuerza\App;
use Fuerza\Utils\Render;

require_once __DIR__ ."/vendor/autoload.php";

App::boot();

/**
 * Function responsible for including the view in the template.
 *
 * @param string $view View to be included.
 * @param array  $args Optional arguments.
 * @return void
 */
function view( string $view, array $args = [] ): void {

	Render::view( $view, $args );

}
