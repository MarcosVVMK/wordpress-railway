<?php

return [
	'providers' => [
		\Fuerza\Providers\AssetServiceProvider::class,
		\Fuerza\Providers\MenuServiceProvider::class,
		\Fuerza\Providers\ThemeServiceProvider::class,
		\Fuerza\Providers\WidgetsServiceProvider::class,
		\Fuerza\Providers\ViewComposerServiceProvider::class,
		\Fuerza\Providers\JWTServiceProvider::class,
		\Fuerza\Providers\RouteServiceProvider::class,
		\Fuerza\Providers\EloquentServiceProvider::class,
	],

	'postTypes' => [
		\Fuerza\Providers\PostTypes\ExamplePostType::class,
	],

	'taxonomies' => [
		\Fuerza\Providers\Taxonomies\ExampleCategory::class,
	],

	'middlewares' => [
		'token' => \Fuerza\Middleware\Token::class,
		'nonce' => \Fuerza\Middleware\Nonce::class,
	],
];
