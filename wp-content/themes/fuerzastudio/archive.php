<?php
/**
 * Archive Template
 *
 * The template for displaying archives (categories, tags, author, date).
 *
 * @package Fuerza
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <header class="page-header">
            <?php
            if (is_category()) {
                echo '<h1 class="page-title">' . single_cat_title('', false) . '</h1>';
            } elseif (is_tag()) {
                echo '<h1 class="page-title">' . single_tag_title('', false) . '</h1>';
            } elseif (is_author()) {
                the_post();
                echo '<h1 class="page-title">' . get_the_author() . '</h1>';
                rewind_posts();
            } elseif (is_day()) {
                echo '<h1 class="page-title">' . get_the_date() . '</h1>';
            } elseif (is_month()) {
                echo '<h1 class="page-title">' . get_the_date('F Y') . '</h1>';
            } elseif (is_year()) {
                echo '<h1 class="page-title">' . get_the_date('Y') . '</h1>';
            } else {
                echo '<h1 class="page-title">' . __('Archives', 'fuerza') . '</h1>';
            }
            ?>
        </header>

        <?php
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h2 class="entry-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    </header>

                    <div class="entry-content">
                        <?php the_excerpt(); ?>
                    </div>
                </article>
                <?php
            endwhile;

            the_posts_pagination();

        else :
            ?>
            <p><?php _e('No posts found.', 'fuerza-studio'); ?></p>
            <?php
        endif;
        ?>

    </main>
</div>

<?php
get_sidebar();
get_footer();
